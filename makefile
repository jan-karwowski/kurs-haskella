all:
	rm -r public
	hugo --minify

install: all
	rsync -ac --delete public/ hostido:domains/kurshaskella.jan.karwowscy.eu/public_html/

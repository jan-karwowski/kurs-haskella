---
title: "Wyrażenia"
date: 2024-02-10T11:03:02+01:00
draft: false
---

W większości języków programowania w kodzie programu można wyróżnić wyrażenia i instrukcje. Wyrażenia to elementy, które opisują sposób wyliczenia wartości, a instrukcje służą do definiowania kolejności wykonywania fragmentów programu. Definicje mogą się wydawać proste, ale w praktyce, w wielu językach programowania podziały te są dość śliskie. Spróbuję to pokazać na kilku przykładach z C++.

## Przykłady wyrażeń

Najprostszymi typami wyrażeń są wyrażenia arytmetyczne (występujące nie tylko w C++), czyli ciągi znaków złożonych z operatorów i ich argumentów. Przykłady:

 - `3+4`
 - `4+(3*4)`
 - `3+3*5`
 
Innym typem wyrażenia prostego jest wywołanie funkcji:

 - `sqrt(2)`
 - `std::max(2,1)`
 
Nieco bardziej złożonym wyrażeniem jest operator trójargumentowy:

 - `a == 3 ? "x" : "y"`
 
 Ogólnie, wspólną cechą tych wszystkich bytów jest to, że ich wyliczenie dostarcza wartość, która może być dalej użyta jako np. argument funkcji. (Co prawda w tym momencie można zauważyć, że w C++ istnieją wyrażenia void, które nie dostarczają wartości. Jest to dziwactwo C++, które moim zdaniem jest błędem projektowym, ale musimy z tym żyć. Na potrzeby definicji ogólnej załóżmy, że wyrażenia void nie istnieją).
 
## Przykłady instrukcji

Podstawowym i najczęściej używanym rodzajem instrukcji w C++ są wyrażenia zakończone średnikiem:

- `printf("%d", 12);`
- `sqrt(2);`
- `3+4;`

Różnica w stosunku do wyrażenia jest taka, że wpisy zakończone średnikiem będą wykonywane przez program w takiej kolejności jak występują w liniach, jedna po drugiej. Stąd też są instrukcjami, bo odpowiadają za kolejność wykonania kodu.

Najbardziej naturalnymi instrukcjami są instrukcje warunkowe i pętle, które definiują który kod zostanie wykonany i ile razy powtórzony. Przykłady:

```C++
if(x == -1) {
    return "not found";
}
```


```C++
for(int i=0; i< 30; i++) {
    v.push_back(3+i);
}
```


Instrukcja nie może wystąpić na przykład jako parametr funkcji. 

## Inne elementy składni

Każdy język programowania ma jakieś elementy składni, które nie są bezpośrednio związanie z wyliczaniem wyniku programu, na przykład deklaracje przestrzeni nazw, deklaracje typów funkcji, itp. Są to elementy, nad którymi nie będziemy się teraz pochylać.

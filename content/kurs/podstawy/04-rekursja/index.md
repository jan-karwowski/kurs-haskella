---
title: "Lekcja 4: Rekursja"
date: 2023-10-31T16:54:05+01:00
lessonnumber: 4
draft: false
---

## Pierwsze większe funkcje

Dotychczas pisaliśmy jedynie bardzo krótkie i bardzo proste funkcje, a lekcje miałby na celu bardziej przybliżenie składni definiowania funkcji. W tym momencie dysponujemy już arsenałem składniowym, który pozwoli na rozwiązywanie ciekawszych problemów. W imperatywnych językach programowania jednym z podstawowych mechanizmów pozwalającym na pisanie złożonych programów jest pętla. W Haskellu nie ma pętli, bo nie ma zmiennych (zmiana wartości zmiennej jest w zasadzie jedynym sposobem na przerwanie pętli w języku imperatywnym), zamiast pętli używana jest rekursja, czyli wywoływanie funkcji przez samą siebie. Zastąpienie pętli przez rekursję nie zmienia tego co da się wyliczyć -- każdy program z użyciem pętli można przepisać z użyciem rekursji, użycie rekursji w Haskellu wymaga jednak ostrożności, bo niewłaściwie skonstruowana rekursja będzie skutkowała bardzo złą złożonością pamięciową programu. Tym jak zadbać o złożoność pamięciową, zajmiemy się [za jakiś czas]({{< ref "../11-rekursja-ogonkowa/index.md" >}}). W trakcie tej lekcji będzie nas interesował tylko wynik funkcji, nawet jeśli sposób obliczenia nie jest optymalny.

## Wyliczanie silni 

Zacznijmy od prostego problemu -- wyliczenie silni. Silnia w matematyce to funkcja, która każdej liczbie dodatniej k przypisuje iloczyn \(k \cdot (k-1) \cdot (k-2) \cdots 1\). Czyli silnia liczby 1 to 1, silnia liczby 2 to `2*1=2`, 3 silnia to `3*2*1=6`, itd. W matematyce silnię oznacza się wykrzyknikiem po liczbie: `5! = 5*4*3*2*1`. Od razu widać, że dla coraz większych liczby wymagane jest wykonanie coraz większej liczby mnożeń. Drugim ważnym spostrzeżeniem, które można zrobić jest to, że silnia liczby `k` to `(k-1)!*k`. Pozwolę sobie zapisać ten wzór w formie funkcji w Haskellu:
```haskell
{-# OPTIONS -Wall #-}
module Lekcja 4 where

factorial :: Int -> Int
factorial k = k * (factorial (k-1))
```
Jak zwykle zapisuję mój plik, tym razem `Lekcja4.hs`, uruchamiam interpreter i wyliczam wartość `factorial 3`. I? I nie stało się nic dobrego. W zależności od konfiguracji komputera i ghci mogliście albo zaobserwować błąd `stack overflow` albo komputer praktycznie przestał odpowiadać. Co się stało?

Problemem jest to, że moja rekurencja nie ma zdefiniowanego żadnego warunku brzegowego, więc:
```
factorial 3 = 3 * (factorial 2) =
= 3 * 2 * (factorial 1) = 
= 3 * 2 * 1 * (factorial 0) =
= 3 * 2 * 1 * 0 * (factorial (-1)) =
...
```
Widać, że ta definicja będzie rozwijać się w nieskończoność. 

Żeby poprawić moją funkcję muszę, jak w każdej rekurencji, zdefiniować warunek brzegowy, w tym przypadku silnię dla wartości 1:
```haskell
factorial :: Int -> Int
factorial 1 = 1
factorial k = k * (factorial (k-1))
```
Tak zdefiniowana silnia działa dla wszystkich liczb dodatnich, sprawdzam dla wartości 2, 3 i 4. Jest OK. Oczywiście gdy wyliczę silnię dla liczby niedodatniej, problem się powtarza. Tu zderzam się z problemem matematycznym -- silnia nie jest zdefiniowana dla liczb niedodatnich. Póki co nie poznaliśmy jeszcze techniki raportowania błędnych argumentów funkcji, więc przyjmiemy, że argumenty niedotatnie są OK, a wartość silni dla dowolnej takiej liczby wynosi 0. Żeby zdefiniować coś takiego, muszę wycofać się z różnych definicji dla konkretnych wartości argumentów i wykorzystać wartowników:
```haskell
factorial :: Int -> Int
factorial k 
    | k < 1 = 0
    | k == 1 = 1
    | otherwise = k * (factorial (k-1))
```
Tak zdefiniowana silnia już nie powoduje problemów. Zwraca oczekiwany wynik dla liczby dodatnich, ujemnych oraz 0.

W tym kodzie można zrobić jeszcze jedną kosmetyczną poprawkę: aplikacja funkcji ma wyższy priorytet niż jakikolwiek operator w Haskellu. Można zatem opuścić nawiasy dookoła wyliczenia wartości `factorial`:
```haskell
factorial :: Int -> Int
factorial k 
    | k < 1 = 0
    | k == 1 = 1
    | otherwise = k * factorial (k-1)
```
Nawiasów dookoła `k-1` nie można opuścić, bo aplikacja funkcji ma wyższy priorytet niż operator `-`, zatem wyrażenie `factorial k-1` zostanie zinterpretowane jako
`(factorial k)-1`.


## Problem 2 -- liczby Fibonacciego

Rozważmy trudniejszy przykład, funkcję wyliczającą liczby Fibonacciego. Liczby te są zdefiniowane w następujący sposób:
```
Fib(0) = 1
Fib(1) = 1
Fib(n) = Fib(n-2) + Fib(n-1)
```
Taką definicję mogę w naiwny sposób przepisać do Haskella:
```haskell
fib :: Int -> Int
fib k 
    | k == 0 = 1
    | k == 1 = 1
    | k > 1 = (fib (k-2)) + (fib (k-1))
    | otherwise = 0
```
Pozwoliłem sobie na przypisanie wartości 0 w przypadku, gdy argumentem jest liczba 0.
Taka funkcja technicznie działa -- wyliczy wartość danej liczby Fibonacciego. Jednak każdy, kto wyliczał liczby Fibonacciego w innym języku programowania, wie, że takie podejście jest wolne. Dlaczego? Wystarczy rozpisać wzór na `fib (k-1)`: `fib (k-1) = fib (k-2) + fib (k-3)` . Jeśli teraz podstawię to do definicji funkcji `fib` dla `k>1`, to otrzymuję następujące wyrażenie: `(fib (k-2)) + (fib (k-1)) = (fib (k-2)) + (fib (k-2) + fib (k-3))`. Od razu rzuca się w oczy dwukrotne wyliczenie wartości `fib (k-2)`. Jeśli zaczniemy to rozpisywać dalej, to okaże się, że w wyliczeniu `fib (k-2)` jest użyte `fib (k-3)` -- ponownie następuje wielokrotne wyliczanie tej samej wartości.
Oczywiste jest, że szybciej jest jeśli jedną wartości wyliczymy jeden raz, niż jak wyliczamy ją kilka razy. Przynajmniej w typowych językach programowania.
W przypadku Haskella tak być nie musi, z racji tego, że mamy tu do czynienia z funkcjami "czystymi", czyli takimi bez efektów ubocznych, środowisko uruchomieniowe *może* (zwracam uwagę, że nie musi i zależy to od wielu czynników), zapamiętywać raz wyliczoną wartość funkcji na wypadek, gdyby potrzebne było ponowne wyliczenie tej wartości.
Niemniej jednak nie ma takiej gwarancji. Co więcej, już za kilka lekcji pokażę, że taki sposób wyliczania wartości, podobnie jak w przypadku silni ma słabą złożoność pamięciową. Zastanowię się więc, czy potrafię jakoś ten problem rozwiązać. 

Obserwacja, która pomoże nam poprawić to rozwiązanie, to fakt, że żeby wyliczyć `Fib(k)`, potrzebujemy wyliczyć wszystkie wartości `Fib(x)` dla `x=k-1, k-2, ..., 2`, czyli dla wszystkich wartości mniejszych od `k`. Wynika to z faktu, że aplikując wzór rekurencyjny `Fib` wyliczamy wartości `Fib` dla argumentu mniejszego o `1`. 
W związku z tym, zamiast liczyć liczbę Fibonacciego "od `k` do `1`, spróbujemy liczyć odwrotnie, czyli od `1` do `k`. 

Plan działania: będę wyliczał liczby Fibonacciego parami, najpierw parę Fib(0) i Fib(1), potem Fib(1) i Fib(2), o tak dalej, aż do momentu, gdy w wyliczonej parze będzie liczba, której szukam. To pozornie bardziej skomplikowane rozwiązanie pozwoli mi na wyliczenie każdej liczby Fibonacciego dokładnie raz, zamist liczyć artości wielokrotnie. Zdefinuję funkcję o takiej sygnaturze: 

```haskell
fibhelper :: Int -> Int -> Int -> Int -> Int
```
Funkcja jest czteroargumentowa, znaczenie argumentów:

1. Numer liczby Fibonacciego, której szukamy. Jeśli chcę obliczyć `Fib(5)`, to wartość tego argumentu jest 5. Oznaczę tę wartość jako `n`.
2. Pierwsza z wyliczonej aktualnie pary liczb Fibonacciego, oznaczę ją jako `Fib(k-1)`, w kodzie będzę używał `fib_k_1`
3. Druga z wyliczonej aktualnie pary liczb Fibonacciego, onz. `Fib(k)`, w kodzie `fib_k`
4. Informacja o numerze wyliczonych aktualnie liczb Fibonacciego -- `k`.

Zarys algorytmu: Wystartuję z wartościami `Fib(0), Fib(1), k=1`, w każdym kroku będę wyliczał kolejną parę liczb aż dojdę do sytuacji, że `k==n`. Wtedy po prostu zwrócę już wyliczone `Fib(k)`. Implementacja, część pierwsza:
```haskell
fibhelper n fib_k_1 fib_k k 
    | k == n = fib_k
    | otherwise = undefined
```
Na razie zaimplementowałem warunek końcowy mojego algorytmu -- gdy k == n, zwróć wynik. W miejsce głównej części obliczenia, czyli wyliczenie kolejnych par, wpisałem na razie wartość `undefined`. Jest to wartość, której wyliczenie powoduje błąd wykonania. Zrobiłem to po to, żeby moja częściowa implementacja, która jeszcze nie ma prawa zadziałać, kompilowała się. Teraz czas na drugą część, czyli wyliczenie kolejnej pary liczb Fibonacciego. To nie będzie trudne, bo jeśli zwiększę `k` o 1, to większa z moich liczb, czyli `fib_k`, stanie się `fib_k_1` w kolejnej iteracjo, a nowe `fib_k`, to suma starych `fib_k + fib_k_1`. Podsumowując, przejście do kolejnego kroku, to podstawienia: `k ← k_old+1`, `fib_k ← fib_k_old + fib_k_1_old`, `fib_k_1 ← fib_k_old`. Przyrostkiem `_old` oznaczyłem wartości z poprzednich iteracji. 
Pozostaje przekuć to na implementację w Haskellu:

```haskell
fibhelper n fib_k_1 fib_k k 
    | k == n = fib_k
    | otherwise = fibhelper n fib_k (fib_k + fib_k_1) (k + 1)
```
Implementacja, po przeprowadzeniu wcześniejszego rozważania jest prosta, wystarczy rekurencyjne wywołanie fibhelper z nowymi wartościami argumentów. Przypominam o konieczności otoczenia argumentów funkcji będących złożonymi wyrażeniami nawiasami, w przeciwnym wypadku w składni Haskella każde z wyrażeń będzie oddzielnym argumentem funkcji fibhelper, bo aplikacja funkcji wiąże najsilniej.

To rozwiązanie działa, ale jest toporne. Żeby wyliczyć `Fib(9)` muszę wyliczyć wartość:

```haskell
fibhelper 9 1 1 1
```
a żeby wyliczyć `Fib(5)`:
```haskell
fibhelper 5 1 1 1
```
Trzy ostatnie argumenty funkcji są zawsze takie same, gdybym podał inne, to otrzymam wynik dla innego ciągu niż ciąg Fibonacciego. Aby ułatwić życie użytkownikom mojej funkcji, mogę zrobić funkcję pomocniczą, która zwolni wywołującego ze żmudnego obowiązku podawania tych dodatkowych argumentów:
```haskell
fib2 :: Int -> Int
fib2 n = fibhelper n 1 1 1
```

## Podsumowanie

W ramach tej lekcji poznaliśmy podstawowy sposób liczenia bardziej złożonych wartości w Haskellu -- rekursję. 

## Ćwiczenia

1. Napisać funkcję, która wylicza wartość największego wspólnego dzielnika dwu liczb z użyciem algorytmu Euklidesa.

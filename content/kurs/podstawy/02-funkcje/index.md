---
title: "Lekcja 2: Funkcje"
date: 2023-10-19T13:29:22+02:00
lessonnumber: 2
draft: false
---

## Czym jest funkcja?

W językach funkcyjnych, zgodnie z nazwą dużą rolę odgrywają funkcje. Trzeba jednak zwrócić uwagę na to, że w środowisku programistów nazwa funkcja jest nadużywana i zmieniła nieco swoje pierwotne, matematyczne znaczenie. 

Według matematycznej definicji funkcja zdefiniowana jest jako jednoznaczne przypisanie każdej wartości z dziedziny dokładnie jednej wartości z przeciwdziedziny. Innymi słowy chodzi o to, że mamy dziedzinę funkcji, czyli zbiór wszystkich dopuszczalnych argumentów naszej funkcji, przeciwdziedzinę, czyli zbiór dopuszczalnych wartości funkcji i jak weźmiemy jakiś element z dziedziny, to funkcja wskazuje _dokładnie jedną_ wartość w przeciwdziedzinie. Gdyby wskazywała dwie wartości albo nie wskazywała żadnej wartości, to nie byłaby funkcją. Gdyby jej definicja zmieniała się w czasie i przed południem dla wartości **x** z dziedziny wskazywała wartość 1, a po południu dla tej samej wartości **x** z dziedziny wskazywała 2, to nie byłaby funkcją. I największym problemem przy przejściu do opisu funkcji w językach programowania jest właśnie jednoznaczność. 

Dla ustalenia uwagi zajmijmy się na chwilę językiem C (choć podobne cechy mają funkcje w prawie wszystkich językach programowania). Rozważmy funkcję `fgetc`
```C
int fgetc(FILE* stream);
```
Funkcja ta przyjmuje jako argument strumień I/O (np. związany z czytaniem bajtów z pliku) i wczytuje jeden bajt z tego pliku (ewentualnie zwraca wartość sygnalizującą koniec pliku lub błąd). Naturalne dla tej funkcji jest to, że jej pierwsze wywołanie zwraca zupełnie inną wartość niż drugie wywołanie, mimo, że została wywołana w obu przypadkach z tym samym argumentem. Godzi to we wspomnianą wyżej matematyczną definicję funkcji, która wymaga jednoznaczności zwracanej wartości. Listę niezgodności można kontynuować dalej, patrząc na przykład na funkcję `rewind`, która przyjmuje strumień I/O, z którego można czytać, i przewija go do początku, czyli powoduje, że następny odczyt będzie wykonany do pierwszego bajtu:
```C
void rewind(FILE *stream);
```
Ta funkcja w ogóle nie ma przeciwdziedziny, czyli nie zwraca żadnej wartości. W matematyce taki byt byłby bezużyteczny, więc definicja funkcji nie dopuszcza czegoś takiego. 

To wszystko dzieje się dlatego, że w większości języków programowania funkcjom dodano dodatkową odpowiedzialność, której w matematyce nie było --- interakcję ze światem na zewnątrz programu. I tak różne wartości zwracane przez funkcje w języku C biorą się z tego, że poza programem, na przykład w zewnętrznym pliku, znajdują się różne bajty. Dlatego funkcje operujące na strumieniach zwracają różne wartości dla kolejnych wywołań. Funkcje typu `void` z kolei służą do zrobienia jakiejś zmiany w poza programem, np. wypisania komunikatu na ekran. Jeśli spojrzymy na funkcje, które nie mają tego typu odpowiedzialności, na przykład na funkcję `sqrt`, która wylicza pierwiastek kwadratowy
```C
double sqrt(double x);
```
to taka funkcja pasuje do matematycznej definicji. Wartości pierwiastków kwadratowych nie zmieniają się w czasie.

Trochę innym przypadkiem, nad którym warto się pochylić, jest funkcja `rand`, która zwraca kolejną wartość wyliczoną przez prosty generator liczb pseudolosowych.
```C
int rand();
```
Ta funkcja znów nie pasuje do matematycznej definicji, bo zwraca różne wartości (dla jedynego możliwego układu parametrów będącego pustą krotką). Tu nie ma żadnej komunikacji ze światem poza programem, zamiast tego jest stan przechowywany w pewnej zmiennej. Każde wywołanie `rand()` zmienia wartość tej zmiennej. Do pary istnieje funkcja
```C
void srand(int seed);
```
która ustawia wartość tej zmiennej opisującej stan wewnętrzny generatora --- jej odpowiedzialnością nie jest dostarczenie żadnej wartości, a jedynie zmiana stanu jakiejś zmiennej.

Tu ponownie zderzamy się z tym, że wspomniane funkcje z biblioteki standardowej języka `C` mogą mieć inne odpowiedzialności niż przekształcenie wartości z dziedziny w wartość z przeciwdziedziny. 

### Funkcje czyste

Programiści ze środowisk zgromadzonych wokół języka Haskell w takiej sytuacji będą mówili o funkcjach czystych i nieczystych.

Funkcja czysta
: to taka funkcja, której jedyną odpowiedzialnością jest wyliczenie wartości z przeciwdziedziny dla podanego argumentu z dziedziny. Funkcja taka nie może powodować żadnych _efektów ubocznych_, czyli na przykład zmieniać wartości zmiennych w programie, odczytywać wartości zmiennych zmienianych przez inne funkcje, w jakiś sposób oddziaływać ze światem zewnętrznym, na przykład wypisując komunikaty, czy odczytując wartość zegara.

Funkcja nieczysta
: to taka funkcja, która nie spełnia warunków funkcji czystej.


Funkcje czyste mają kilka własności, które ułatwiają posługiwanie się nimi, warto zwrócić uwagę, że:

 - w gdy chcemy wyliczyć wiele wartości różnych funkcji czystych, kolejność prowadzenia obliczeń nie ma żadnego znaczenia, można je wykonać w dowolnej kolejności, zlecić ich wykonanie kilku wątkom równolegle lub zlecić obliczenia chmurze,
 - analizując poprawność czy działanie programu mamy mniej czynników do uwzględnienia, bo do ustalenia jaka będzie wartość funkcji wystarczy ustalić wartości argumentów. Żadne inne obiekty w programie nie mają na to wpływu.
 - Można dokonywać optymalizacji polegającej na zapamiętywaniu w pamięci podręcznej wartości funkcji dla często używanych argumentów. Wartość ta nigdy się nie dezaktualizuje.
 
## Funkcje w Haskellu

Haskell pozwala na definiowanie tylko i wyłącznie funkcji czystych. To ograniczenie jest wymuszone za pomocą dwóch cech:

 - w Haskellu na poziomie języka nie ma żadnych zmiennych oraz
 - w bibliotece standardowej nie ma funkcji, które wykonywałyby interakcje ze światem zewnętrznym. 
 
Powyższe ograniczenia oznaczają też, że w Haskellu nie ma w ogóle pojęcia instrukcji. Pracujemy tylko z wyrażeniami.
Jeśli ktoś nie spotkał się z pojęciem instrukcji i wyrażenia w trakcie swojej dotychczasowej nauki programowania, to proszę o przeczytanie [krótkiego objaśnienia]({{<ref "/apendix/wyrazenia.md" >}}).
 
Brak funkcji nieczystych jest pierwszą charakterystyczną cechą Haskella. Ogromna większość języków dopuszcza definiowanie takich funkcji, są one w zasadzie niezbędne do tego, żeby program zakomunikował swój wynik. Zasadnym pytaniem jest jak program w Haskellu komunikuje się ze światem zewnętrznym. Ze względu na to, że ten kurs Haskella jest powolny, odpowiedź na to pytanie znajduje się w dużo dalszej części kursu. Na razie będziemy tylko definiować funkcje czyste. 

### Składnia definiowania funkcji

Oprócz dużej różnicy koncepcyjnej w porównaniu z innymi językami programowania, jest też różnica składniowa. Definicje funkcji w Haskellu są niepodobne do tych znanych z wielu innych języków programowania. Jest to dużo mniejszą trudnością w nauce, niż zmiana sposobu myślenia o funkcjach w ogóle, ale mimo wszystko wymaga uwagi i ostrożności gdy rozpoczynamy przygodę z Haskellem. Pierwszą różnicą w stosunku do współczesnych języków jest to, że oddzielnie deklarujemy typ funkcji (czyli typ argumentów i wyniku), a oddzielnie piszemy definicję funkcji. Co więcej typ funkcji jest opcjonalny --- gdy go pominiemy kompilator sam wyznaczy typ funkcji w trakcie kompilacji. 

Zacznijmy od składni deklaracji typu funkcji:
```haskell
addone :: Int -> Int
```

Powyższy kawałek kodu czytamy w następujący sposób: funkcja `addone` (addone to jest nazwa funkcji) przekształca wartość typu `Int` w wartość typu `Int`. Przyglądając się uważnie widzimy następujące elementy składni języka:
 - identyfikator addone --- nazwa funkcji. Nazwa funkcji może być dowolnym napisem rozpoczynającym się **małą literą**. Wielkość litery którą rozpoczyna się nazwa bytu w Haskellu ma znaczenie składniowe. To też jest niespotykane w innych językach, gdzie zwykle są konwencje dotyczące nazewnictwa, ale nie są one wymuszane. 
 - podwójny dwukropek --- element składni języka, który oddzielna nazwę funkcji od typu Spacje dookoła dwukropka nie są obowiązkowe, ale ogólnie przyjęty styl formatowania kodu zakłada, że te odstępy są.
 - typ argumentu funkcji, tu `Int` (choć ani trochę nie musi być to ten sam typ co argument). Typy danych, dla odróżnienia od nazw funkcji są nazywane napisami rozpoczynającymi się **wielką literą**.
 - strzałka `->` (złożona z dwu znaków: `-` oraz `>`. Separuje typ argumentu od typu wyniku. Podobnie jak przy dwukropku spacje dookoła są opcjonalne.
 - typ wyniku funkcji, tu `Int`.
 
Uważny czytelnik zauważy w tym miejscu, że mówię o argumencie funkcji, a nie o _pierwszym_ argumencie funkcji, tak jakby nie było możliwości zdefiniowania funkcji więcej niż jednego argumentu. O tym więcej już za chwilę. Na razie zostańmy przy funkcji jednoargumentowej.

Sam typ nie wystarczy żeby było wiadomo jak wyliczyć wartość funkcji. Jako przykład wybrałem funkcję `addone`, która ma dodawać jedynkę do podanego argumentu. Dlatego każdej deklaracji typu funkcji musi towarzyszyć definicja, czyli wyrażenie mówiące jak obliczyć wartość danej funkcji: 
```haskell
addone x = x + 1
```
Definicja składa się z następujących elementów:
 - nazwa funkcji, tu `addone`,
 - spacja; tu ważny komentarz: w Haskellu pomiędzy nazwą funkcji a nazwą argumentu używa się ani nawiasu ani przecinka, ani innego podobnego separatora. Używa się odstępu i niczego więcej. Jest to zupełnie inne niz w większości języków programowania i trzeba się do tego przyzwyczaić
 - nazwa argumentu, tu `x` --- dowolny identyfikator, którego nazwa rozpoczyna się **małą literą**, będzie użyty do odwołania się do wartości argumentu w wyrażeniu wyliczającym wartość funkcji
 - `=` --- znak równości oddziela nazwę i argument funkcji od wyrażenia definiującego jak obliczyć wartość funkcji
 - wyrażenie obliczające wartość funkcji, tu `x+1`
 
To cała definicja funkcji wraz z typem. Teraz czas na sprawdzenie działania:

1. Otwieramy skonfigurowany w ramach poprzedniej lekcji edytor i tworzymy w nim nowy plik z rozszerzeniem `.hs`. W tym momencie nazwa pliku nie ma jeszcze większego znaczenia. Pozwolę sobie nazwać mój plik `Lekcja1.hs`
2. Wewnątrz pliku będzie trzeba zdefiniować moduł -- każda funkcja musi być częścią jakiegoś modułu, o modułach opowiem w późniejszych lekcjach. Na razie ważne jest tylko aby w pierwszej linii pliku znalazł się napis `module Lekcja1 where`, czyli cały plik powinien wyglądać następująco:
   ```haskell
   module Lekcja1 where
   
   addone :: Int -> Int
   addone x = x + 1
   ```
   po definicji modułu wpisałem zaprezentowany wcześniej kod definiujący funkcję.
3. Zapisuję zmiany w moim pliku.
4. Teraz trzeba przejść do testowania tej funkcji. Przez pierwsze lekcje będziemy testować nasze funkcje w interpreterze `ghci`,
5. Jeśli wybrany edytor umożliwia po prostu uruchomienie interpretera `ghci` i załadowanie bieżącego pliku, to możemy wykorzystać taką funkcję środowiska. Jeśli takiej opcji nigdzie nie ma, to:
   1. Uruchamiam terminal i przechodzę do katalogu, gdzie znajduje się utworzony przeze mnie plik
   2. Wydaję polecenie `ghci`
   3. Uruchomienie interpretera powinno charakterystycznym komunikatem
   4. Wydaję polecenie nakazujące interpreterowi załadowanie mojego pliku: `:l Lekcja1.hs`
   5. Powinienem otrzymać komunikat, że moduł został załadowany. Od tego momentu mogę wyliczać wartości mojej funkcji `addone`
6. Wyliczenie wartkości funkcji: w większości języków programowania wyliczenie wartości opisuje się jako podanie nazwy funkcji i argumentów w nawiasach, na przykład `addone(2)`. W Haskellu składnia jest inna: aplikację funkcji (czyli wyliczenie wartości dla zadanego argumentu) zapisuje się poprzez napisanie nazwy funkcji, a potem wartości argumentu po **spacji**. Znów nie pojawiają się żadne dodatkowe separatory. Testuję więc moją funkcję dla kilku wartości, wpisując w interpreterze:
	  - `addone 1`
	  - `addone 8`
	  - `addone (-1)` 
	  
	  Warto zauważyć, że w ostatnim przypadku musiałem liczbę ujemną podać w nawiasach. Jest tak, bo zapis bez nawiasów zostałby zinterpretowany jako "odejmij od addone wartość 1" i skutkowałby błędem kompilacji, bo nie ma zdefiniowanego operatora odejmowania dla typu funkcja i typu `Int`.

### Funkcje o więcej niż jednym argumencie

Zacznijmy od nieco rozczarowującego stwierdzenia: w Haskellu w zasadzie nie ma funkcji wieloargumentowych, przynajmniej formalnie. W tej sekcji pokażę kawałek składni, który będzie wyglądał jak funkcja dwu (lub więcej) argumentowa, a dopiero potem wytłumaczę, czemu tak nie jest. wieloargumentowych.

Na sam początek zduplikujmy istniejący już operator dodawania. Zdefiniuję funkcję `plus`, która będzie po prostu dodawać do siebie wartość pierwszego i drugiego argumentu. Typ funkcji wygląda w następujący sposób:
```haskell
plus :: Int -> Int -> Int
```
Pojawił się po prostu dodatkowy typ i strzałka. Pierwsze dwa `Int` to argumenty, a ostatni to typ wyniku.

W praktyce jest, co prawda możliwe, (do tego dojdziemy w kolejnych lekcjach), zdefiniowanie funkcji zwracającej funkcję, ale w większości przypadków rzeczywiście chodzi nam o analogon funkcji dwuargumentowej. W związku z tym podstawowa składnia Haskella pozwala nam zdefiniować właśnie ostateczną wartość po zaaplikowaniu kolejnych argumentów (tu dwóch).

```haskell
plus x y = x + y
```
Jedyną różnicą jest to, że po nazwie funkcji `plus` występują tym razem dwie nazwy argumentów oddzielone spacjami. 

Pozostaje sprawdzić czy zdefiniowana funkcja działa. W tym celu dopisuję do utworzonego wcześniej pliku `Lekcja1.hs`, pełna zawartość pliku teraz:
```haskell
module Lekcja1 where
   
addone :: Int -> Int
addone x = x + 1

plus :: Int -> Int -> Int
plus x y = x + y
```

Definicja, podobnie jak deklaracja, różni się tym, że pojawił się drugi argument. Po nazwie funkcji, `+`, następują dwa identyfikatory, odpowiednio dla pierwszego i dla drugiego argumentu, oddzielone spacją (to ważne, w prawie wszystkich językach używa się przecinków, tu odstępów). Następnie znak równości i definicja wartości funkcji.

### Tak naprawdę to są dalej funkcje jednoargumentowe

Jak wspomniałem, w rzeczywistości, funkcja wieloargumentowa jest tylko pozorna. W zasadzie, jest to lukier składniowy uławiający definiowanie złożonych funkcji. Przejdźmy przez funkcję `plus` raz jeszcze:

```haskell
plus :: Int -> (Int -> Int)
```

Tym raze dopisałem nawiasy. Te nawiasy pokrywają się z kolejnośćią w jakiej kompilator interpretuje strzałki oddzielające argumenty funkcji przy parsowaniu programu (inaczej: te strzałki są *prawostronnie łączne*).
Gdyby funkcja miała więcej argumentów, to interpretowana byłaby w taki sposób:
```haskell
fun :: A -> (B -> (C -> D))
```

W wersji bez nawiasów, narzucająca się interpretacja to funkcja która przyjmuje trzy argumenty typów A, B, C, a jej wynikiem jest D. Po wstawieniu nawiasów naturalną interpretacją jest to, że mamy funkcję, któa dla argumetnu A zwaca pewną funkcję. Ta pewna funkcja to funkcja, która dla argumentu B zwraca jezcze iiną funkcję. A ta jeszcze inna funkcja ma tę cechę, że dla argumentu C zwraca wartość D. Ujawniłem tu jeszcze jedną cechę Haskella, która jest dość powszechna we współczesnych językach programowania: funkcja jest typem danych, takim samym jak np. Int, czy Bool (starsze języki, jak np. C, zwykle traktują funkcję jak specjalny byt).

Taka interpretacja jest właściwa. Jeśli wrócimy do zdefiniowanej niedawno funkcji plus, to dwa poniżse wyrażenia dadzą dokłądnie tę samą wartość:
```haskell
addone 3
(plus 1) 3
```

Innymi słowy, wyrażenie `(plus 1)` to funkcja jednoargumentowa. Jej wynikiem jest zaaplikowanie funkcji plus dla dwu argumentów: 1 oraz argumetnu podanego (w przykładdzie 3). 


### Nawiasy i łączność -- podsumowanie

W praktycznie każdym języku programowania używamy nawiasów, żeby zapewnić właściwą kolejność obliczeń, na przykład wyrażenie `(3+4)*5` da inny wynik niż `3+4*5`. W składni każdego języka zdefiniowane są priorytety operatorów i ich łączność. W tej lekcji pojawiły się daw elementy związane z łącznośćią: 
 - Strzałka oddzielająca kolejne typy argumentów ma łączność _prawostronną_. Oznacza to, że wyrażenie `A -> B -> C -> D` jest interpretowane jako `A -> (B -> (C -> D)))`. W związku z tym w deklaracji typu funkcji `plus` możemy pominąć nawiasy lub ococzyć nimi skrajną prawą parę argumentów, oba warianty są równoważne.:
```haskell
plus :: Int -> Int -> Int
plus :: Int -> (Int -> Int)
```
 - Aplikacja funkcji z kolei ma łączność lewostronną, czyli wyrażenie `x y z w` jest równoważne `((x y) z) w`, co oznacza, że możemy wyliczając wartość funkcji w interpreterze pisać `plus 3 2` równoważnie z `(plus 3) 2`. 

## Podsumowanie

To koniec pierwszej lekcji. Dość długa, a w sumie bardzo mało informacji. W następnej lekcji kontynuowany będzie temat definicji funkcji, wprowadzone zostaną bardziej zaawansowane techniki definiowania funkcji. Najważniejsze informacje, które były omówione w tej lekcji:

 - w Haskellu wszystkie funkcje są jednoargumentowe,
 - składnia definiowania funkcji jest niepodobna do popularnych języków programowania,
   - typ funkcji deklarowany jest przez `nazwa :: Arg1 -> Arg2 -> Wynik`,
   - funkcja jest definiowana przez `nazwa x y = wyrażenie liczące wartość funkcji`,
 - wielkość liter ma znaczenie. Nazwy funkcji i argumentów muszą zaczynać się małą literą, a nazwy typów danych muszą zaczynać się wielką literą.

## Zadania

Spróbuj napisać funkcję `sum3`, która będzie dodawać wartości swoich 3 argumentów oraz funkcję `sum4`, która będzie dodawać wartości 4 argumentów. 

Mając napisane `sum4` spróbuj zapisać definicję funkcji `sum3` używając funkcji `sum4` zamiast operatora `+`.

---
title: "Lekcja 5 -- Leniwe obliczanie"
date: 2025-01-06T20:50:09+01:00
draft: true
lessonnumber: 5
---

 Sztandarową cechą Haskela, wymienianą prawie zawsze na początku cech języka, jest leniwe obliczanie.
Jednocześnie leniwe obliczanie jest też chyba najczęśćiej krytykowaną cechą Haskella. 

Leniwe obliczanie polega na tym, że wartości parametrów i innych definicji elementów są wyliczane dopiero wtedy, kiedy ich wartość jest konieczna do wyliczenia wyniku.
Jest to inne podejście niż w typowych językach programowania takich jak np. C++, gdzie przed przekazaniem argumentów do funkcji wyliczane są ich wartości.
Na przykład, jeśli zrobimy funkcję, która przyjmuje cztery argumenty: a, b, c, d, i zdefiniujemy ją tak, że jeśli `a>b`, to wynikiem jest `c`, a w przeciwnym wypadku `d`.
W języku C najpierw muszą zostać wyliczone a, b, c i d, a dopiero potem zostanie sprawdzone, czy a jest większe od b i odpowiednio zostanie zwrócona wartość c lub d. W zależności od ścieżki wykonania, wartość c albo d zostanie zmarnowana. 
W Haskelu wartości argumentów są wyliczane dopiero, gdy zostaną użyte. W przypadku naszego przykładu najpierw zostaną obliczone wartośći a i b, ponieważ są one konieczne do ustalenia czy a>b. Dopiero potem, w zależności od wyniku tego porównania zostanie wyliczone c lub d. W przyapdku, gdy wyliczenie c i d wymaga dużego nakładu obliczeniowego, może to zaoszczędzić sporo czasu.

Jednak leniwe obliczanie to nie same zalety. Najważniejszą wadą, którą będziemy omawiać w dalszej części kursu, jest fakt, że leniwe obliczanie znacznie utrudnia kontrolę nad tym ile program zużywa pamięci. Mniejszym problemem jest to, że czasam leniwe obliczanie ukrywa błędy, które wystąpiły w innych miejscach (a w zasadzie nie wystapiły, bo wartości powodujące błąd nie zostały wyliczone).

TODO

jak mówimy o języku Haskell
 i ma trochę zalet i trochę wad
 tak naprawdę jednocześnie język Haskell
 w sumie najczęściej jest krytykowany za to
 że jest w nim leniwe obliczanie
 w tej chwili skupię się na zaletach leniwego obliczania
 na tym, co one może nam dać
 natomiast w kolejnych lekcjach
 będą sekcje o wadach tego rozwiązania
 jakie są zalety?
 jest jedna oczywista zaleta
 czyli pewne elementy kodu nie są nigdy wyliczane
 w związku z czym program może działać szybciej
 co więcej
 mogą się zdarzyć takie przypadki
 gdzie wyliczenie jakiegoś argumentu potencjalnie kończy się błędem
 my możemy
 takie zachowanie
 symulować na przykład przez funkcję error w Haskelu
 jest to funkcja, która spowoduje
 wywalenie się interpretera z pewnym błędem
 i możemy poeksperymentować
 i zobaczyć, że w przypadku leniwego obliczania
 czasami taki błąd się nigdy nie ujawni
 co niekoniecznie jest plusem
 ale jest też istotną cechą
 tak samo za pomocą tej funkcji
 najłatwiej jest sprawdzić
 że faktycznie pewne elementy
 pewne argumenty się nigdy nie wyliczyły

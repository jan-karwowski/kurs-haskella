{-# OPTIONS -Wall #-}
module Lekcja7 where 

data Point = Pt Double Double

myPoint :: Point
myPoint = Pt 1.0 2.0

getX :: Point -> Double
getX (Pt x y) = x

getY :: Point -> Double
getY (Pt _ y) = y

data Shape = Rectangle Point Point | Circle Point Double

myCircle :: Shape
myCircle = Circle (Pt 1.0 2.0) 5.0
myRect :: Shape
myRect = Rectangle (Pt 1.0 2.0) (Pt 3.0 4.0)

calculateArea :: Shape -> Double
calculateArea (Circle _ r) = pi * r * r
calculateArea (Rectangle (Pt x1 y1) (Pt x2 y2)) = abs ((x1-x2)*(y1-y2))


data Singular = Nullary
data Compass = North | East | South | West
data Empty



---
title: "Lekcja 6: Typy danych"
date: 2024-01-07T13:38:20+01:00
lessonnumber: 7
draft: false
---

## Typy danych

Jak dotąd, korzystaliśmy tylko z typów danych zdefiniowanych języku, najczęściej `Int` i `Double`. Jak każdy język wysokiego poziomu, Haskell pozwala na definiowanie własnych typów danych.

Definicja języka mówi, że w Haskellu definiuje się *algebraiczne typy danych*, ang. *algebraic data types*, w skrócie ADT. Typy takie definiuje się jako **koprodukty produktów typów**. To trudnobrzmiące sformułowanie pochodzi z działu matematyki nazywanego teorią kategorii i, mimo że może brzmieć to groźne, są proste do zrozumienia. 

## Produkty

Zacznijmy od wewnętrznej części definicji czyli "produktów typów". W przypadku typów pojęcie produktu sprowadza się do znanego z nieco mniej abstrakcyjnej matematyki iloczynu kartezjańskiego, schodząc do poziomu języków programowania, odpowiednikiem będzie rekord lub struktura. Jeszcze prościej, produkt dwóch typów, to para wartości `(a,b)`, gdzie `a` jest wartością pierwszego typu, a `b` jest wartością drugiego typu. Produkty możemy definiować dla dowolnej liczby typów rozpoczynając od 0 definiując pary w analogiczny sposób. O ile dla 1, 2, 3, 4 i więcej typów analogia jest chyba jasna, to wątpliwości może budzić pojęcie produktu dla 0 typów, szczególnie wśród osób, które na co dzień nie sypiają z wyższą matematyką. Produkt 0 typów definiujemy jako typ, który ma dokładnie jedną możliwą wartość, nie 0 wartości, nie nieskończenie wiele wartości, a dokładnie 1. W tym momencie może się wydawać dziwne -- po to taki produkt, ale już za chwilę znajdziemy dla niego zastosowanie. 

Jak zdefiniować podstawowy produkt typów? Powiedzmy, że będę chciał zrobić program, który realizuje proste operacje geometryczne na płaszczyźnie. Pierwszy typ, który będzie nam potrzebny, to punkt 2d. Punkt taki składa się z dwóch elementów, współrzędnej `x` typu `Double` oraz współrzędnej `y` typu `Double`.
Definicja typu: 
```haskell
data Point = Pt Double Double
```
Na początku jest słowo kluczowe `data`, które zapowiada definicję typu. Każda definicja będzie rozpoczynać się tym słowem. Następnie podaje się nazwę typu, tutaj `Point` -- jest to nazwa typu, tak jak `Int`, czy `Double` i będzie wykorzystywana na przykład w definicji typów funkcji. Znak `=` oddziela typu od jego definicji. Definicja typu w formie pojedynczego produktu to" wyróżnik -- tutaj `Pt`. Z racji tego, że może istnieć wiele typów będących produktem `Double` i `Double`, żeby w momencie tworzenia wartości można było je rozróżnić, każdy z produktów musi mieć swój unikatowy wyróżnić, tutaj `Pt` (wyróżnik musi być unikatowy pośród wyróżników typów, dopuszczalnej są kolizje z nazwami typów. TO oznacza, że mógłbym nazwać ten produkt `Point` zamiast `Pt`. Wyróżnik typu nazywamy **konstruktorem**. Dla odróżnienia tych dwóch rodzajów bytów, pozostanę jednak na razie przy różnych nazwach. Po wyróżniku następuje zero lub więcej typów wchodzących w skład tego produktu, tu dwa razy `Double`. 

### Użycie produktu

Abu utworzyć wartość zdefiniowanego typu `Point` należy podać wyróżnik produktu oraz wartości wszystkich jego elementów, na przykład
```
Pt 1.0 2.0
```
utworzy punkt o współrzędnych 1,2. 

Oprócz tworzenia punktów, będziemy na pewno definiować funkcje, które przyjmują punkty, jako argumenty. Jak uzyskać wartości współrzędnych takiego punktu? Do współrzędnych, (jak i do wartości składowych jakiegokolwiek innego algebraicznego typu danych), używany jest mechanizm dopasowywania do wzorca (ang. pattern matching). W [Lekcji 3]({{< ref "03-funkcje2" >}}) pojawił się wątek o wielu wariantach definicji funkcji. Pojawiły się różne definicje funkcji dla różnych wartości typu `Int` będących argumentami funkcji. Przypomnę definicję z początku tamtej lekcji: 

```haskell
isZero :: Int -> Bool
isZero 0 = True
isZero x = False
```

`isZero 0` jest najprostszym wariantem dopasowania do wzorca, wzorcem argumentu jest `0`. Teraz poznamy bardziej ogólne formy dopasowań -- dopasowanie do konstruktora argumentu. I tak, możemy napisać funkcję, która pobiera współrzędną `x` punktu: 
```haskell
getX :: Point -> Double
getX (Pt x y) = x
```
mamy tu do czynienia z wzorcem pierwszego argumentu funkcji `(Pt x y)`. Taki wzorzec mówi: ten wariant funkcji ma być stosowany dla wszystkich wartości typu, które zostały skonstruowane konstruktorem `Pt` (w przypadku typu `Point` to oznacza wszystkie wartości w ogóle). Identyfikatory `x` i `y` oznaczają, że w definicji funkcji będzie można się odwoływać poprzez `x` do pierwszej składowej produktu i poprzez `y` do drugiej składowej produktu. 

Podobnie możemy zdefiniować funkcję 
```haskell
getY :: Point -> Double
getY (Pt x y) = y
```

Warto tu zwrócić uwagę, że obie zdefiniowanie funkcje mają tę cechę, że nie używają wszystkich składowych produktu. `getX` nie używa drugiej składowej, a `getY` pierwszej. Można w takiej sytuacji zastąpić nieużywany identyfikator znakiem podkreślenia:
```haskell
getX :: Point -> Double
getX (Pt x _) = x

getY :: Point -> Double
getY (Pt _ y) = y
```

## Koprodukty

Drugą, zewnętrzną częścią sformułowania "koprodukty produktów" są "koprodukty". Koprodukt to alternatywa kilku elementów, tu konkretnie produktów. W języku C istnieje pojęcie unii, które jest zgrubnym odpowiednikiem koproduktu, we w miarę nowym C++ mamy typ `std::variant`. Zdefiniuję teraz przykładowy koprodukt. Pozostając w krainie geometrii 2d, zdefiniuję typ figura geometryczna, który będzie albo kołem albo prostokątem o bokach równoległych do osi układu (technicznie mógłbym zdefiniować dowolny prostokąt, ale prostokąt o takich bokach łatwiej mi będzie zdefiniować).

 - prostokąt będzie zdefiniowany przez dwa punkty -- przeciwległe narożniki
 - koło będzie zdefiniowane przez punkt -- środek oraz promień (`Double`)

Definicja w Haskellu:
```haskell
data Shape = Rectangle Point Point | Circle Point Double
```

Ponownie, mamy słowo kluczowe `data`, następnie nazwę `Shape`. Po znaku równości mamy dwa warianty oddzielone pionową kreską. To właśnie jest definicja koproduktu, dwa warianty opisujące jeden typ. Każdy z wariantów jest produktem: ma konstruktor i typy wchodzące w skład produktu. 

### Użycie koproduktów

W przypadku definiowania wartości typów postępujemy tak samo, jak w przypadku, gdy jest pojedynczy produkt, używamy konstruktora i odpowiednich wartości składowych, na przykład: 

```haskell
Circle (Pt 1.0 2.0) 5.0
```
koło o promieniu 5 i środku (1,2)
```haskell
Rectangle (Pt 1.0 2.0) (Pt 3.0 4.0)
```
prostokąt o podanych narożnikach.

Podobnie również jest w przypadku definiowania funkcji, z tym że tym razem trzeba zdefiniować oddzielny wariant dla każdej z części koproduktu. 
Pokażę to na przykładzie obliczania pola figury: 
```haskell
calculateArea :: Shape -> Double
calculateArea (Circle _ r) = pi * r * r
calculateArea (Rectangle (Pt x1 y1) (Pt x2 y2)) = abs ((x1-x2)*(y1-y2))
```

Musiałem tu zdefiniować dwa warianty funkcji liczącej pole. Po jednym dla każdej figury. Gdybym nie zdefiniował wariantu dla którejś z figur, kompilacja zakończy się sukcesem, ale będzie ostrzeżenie o brakującym wariancie. Gdy program spróbuje obliczyć wartość takiej funkcji dla argumentu, dla którego wariant jest niezdefiniowany, to program zakończy się wyjątkiem. Generalnie, mimo że kompilator zgłasza taki błąd jako ostrzeżenie, należy traktować go jako błąd krytyczny i natychmiast poprawić. 

Pierwszy wariant mojej funkcji `calculateArea` jest zdefiniowany dla koła. W tym przypadku stosuję dopasowanie `(Circle _ r)`. W dopasowaniu używam `_` aby zaznaczyć, że pierwszego elementu produktu nie będę używał, a drugi argument przypinam do identyfikatora `r`. **Uwaga! takie dopasowanie musi być w nawiasie. Inaczej każdy z wyrazów `Circle`,`_`, `r` będzie potraktowany jako odrębny argument funkcji.** Ciekawszy jest drugi wariant, dla prostokąta. Pokazuję tu, że Haksell pozwala na zagnieżdżone dopasowania -- można napisać dopasowanie do całej, głębokiej struktury typu, czyli zarówno do `Rectangle`, jak i do `Point` zawartych w środku. Dzięki temu mogę wykorzystywać identyfikatory `x1`, `y1`, `x2`, `y2`, zamiast używać funkcji `getX` i `getY`. Warto tu też nadmienić, że skorzystałem z funkcji, która nie została wprowadzona wcześniej --- `abs`. Funkcja ta wylicza wartość bezwzględną liczby.

### Produkty nularne

Produkty nularne, czyli produkty, które mają zero elementów, też są używane. Na pierwszy rzut oka może się wydawać, że nie ma dla nich zastosowania, bo co można zrobić z poniższym typem?
```haskell
data Singular = Nullary
```
Ten typ ma dokładnie jedną wartość, zawsze wiadomo jaką. Funkcje, które przyjmują go jako jedyny argument, zawsze będą zwracać ten sam wynik. A jednak nawet w bazowej części języka Haskell jest typ oznaczany symbolem pustych nawiasów`()`, nazywany typem jednostkowym (bo ma tylko jedną wartość). Taki typ pojawi się w użyciu, ale dopiero w bardziej zaawansowanych tematach. W tej części kursu nie spotkamy się z nim.

Niemniej jednak nularne produkty będą się pojawiały, ale nie same. Rozważmy taki typ:
```haskell
data Compass = North | East | South | West
```
Mamy tu typ, który może przyjąć dokładnie cztery wartości. W wielu językach programowania takie proste typy są nazywane typami wyliczeniowymi (enum). W Haskellu nie ma potrzeby specjalnego gatunku typów do opisu wyliczeń, bo wystarczy podstawowy system produktów i koproduktów.

### Typ bez wartości

Dla porządku warto dodać, że da się zdefiniować typ, który nie ma żadnej wartości:
```haskell
data Empty
```
powyższa składnia jest poprawną definicją typu w Haskellu (uwaga na pomięcie znaku równości). Na co dzień nie spotyka się go w użyciu, a sens jego istnienia będzie omówiony w dużo bardziej zaawansowanych częściach kursu.

## Podsumowanie

W tej lekcji opisałem jak można definiować własne typy danych oraz jak definiować funkcje działające na tych danych. Najważniejsze rzeczy to:

 - definicja typu z użyciem słowa kluczowego `data`
 - rozróżnienie pojęcia nazwy typu i konstruktora typu
 - zapamiętanie, że wszystkie typy i konstruktory **muszą** mieć nazwy pisane wielką literą
 - funkcje, które mają różne działanie w zależności od wariantu typu definiuje się poprzez napisanie kilu wariantów funkcji o tej samej nazwie.

[Plik źródłowy z kodem z tej lekcji]({{< resource "Lekcja7.hs" >}})

## Zadania
 
1. Napisać typ danych opisujący działanie matematyczne, jedno z: Suma, Różnica, Iloczyn, Iloraz, Negacja. Pierwsze cztery mają mieć dwa elementy typu Double, piąty jeden element typu Double. Napisać funkcję `wylicz :: Działanie -> Double`, która wyliczy wartość działania oraz funkcję `czySuma :: Działanie -> Bool`, która powie, czy działanie jest różnicą.

---
title: "Lekcja 5: Dodatkowe elementy składni"
date: 2024-01-07T08:13:58+01:00
lessonnumber: 6
draft: false
---

## Usprawnienia składniowe w definicji funkcji

W poprzedniej lekcji zdefiniowaliśmy układ dwu funkcji funkcję `fib2` i `fibhelper`. Funkcja `fib2` jest tylko nakładką na `fibhelper` pozwalającą użytkownikowi tej funkcji na niepodawanie pomocniczych argumentów funkcji `fibhelper`. Przypomnę ten kod:
```haskell
fibhelper :: Int -> Int -> Int -> Int -> Int
fibhelper n fib_k_1 fib_k k 
    | k == n = fib_k
    | otherwise = fibhelper n fib_k (fib_k + fib_k_1) (k + 1)

fib2 :: Int -> Int
fib2 n = fibhelper n 1 1 1

```

Z punktu widzenia kogoś, kto chce wyliczyć wartość jakiejś liczby Fibonacciego funkcja `fibhelper` jest tylko szczegółem implementacji, którego taka osoba nie musi być świadoma istnienia takiej funkcji pomocniczej. Z punktu widzenia osoby, która będzie robić recenzję takiego programu, z kolei, mamy do czynienia z funkcją pomocniczą o dość dużej liczbie argumentów, której przeznaczenie nie jest oczywiste. Nie zostało powiedziane wprost, że to jest tylko element pomocniczy dla `fib2`. Można to częściowo rozwiązać wpisując w kod odpowiednie komentarze, nie mniej jednak w składni Haskella jest kilka dodatkowych elementów pomocniczych. 

### Sekcja where

Pierwszym przydatnym elementem jest sekcja `where`, która służy do zamknięcia definicji pomocniczych dla danej funkcji. Sekcję `where` definiujemy poprzez napisanie `where` po definicji wartości funkcji, a następnie wymienienie wszystkich definicji pomocniczych. Mogę na przykład zawrzeć funkcję `fibhelper` w sekcji `where` funkcji `fib2`:
```haskell
fib2 :: Int -> Int
fib2 n = fibhelper n 1 1 1
  where
    fibhelper :: Int -> Int -> Int -> Int -> Int
    fibhelper m fib_k_1 fib_k k 
      | k == m = fib_k
      | otherwise = fibhelper m fib_k (fib_k + fib_k_1) (k + 1)
```
Aby uniknąć zasłaniania nazw, pozwoliłem sobie przemianować argument `fibhelper` z `n` na `m`.
W powyższym kodzie funkcja `fibhelper` nie jest już funkcją globalną i nie jest widoczna nigdzie poza definicją funkcji `fib2`. Bardzo istotne są wcięcia, składnia Haskella wymaga, żeby funkcje pomocnicze definiowane w ramach where były wcięte. Poza samym porządkowaniem kodu, `where` daje jeszcze jedną istotną funkcjonalność, wartości pomocnicze mogą korzystać wprost z wartości argumentów funkcji okalającej. To znaczy, że mogę skorzystać z wartości argumentu `n` funkcji `fib2` w funkcji `fibhelper`. A to z kolei oznacza, że mogę pozbyć się argumentu `m`, który służy tylko do przekazania żądanej wartości `n`. W efekcie mogę uzyskać taki kod: 
```haskell
fib2 :: Int -> Int
fib2 n = fibhelper n 1 1 1
  where
    fibhelper :: Int -> Int -> Int -> Int
    fibhelper fib_k_1 fib_k k 
      | k == n = fib_k
      | otherwise = fibhelper fib_k (fib_k + fib_k_1) (k + 1)
```
Nie ma żadnych ograniczeń na to, które funkcje mogą posiadać sekcję `where`, może to być każda z funkcji, w tym funkcja będąca w sekcji `where` innej funkcji, mogę zatem trochę zmienić definicję `fibhelper`: 
```haskell
fib2 :: Int -> Int
fib2 n = fibhelper 1 1 1
  where
    fibhelper :: Int -> Int -> Int -> Int
    fibhelper fib_k_1 fib_k k 
      | k == n = fib_k
      | otherwise = fibhelper fib_k fib_k_next k_next
      where
        fib_k_next = fib_k + fib_k_1
        k_next = k + 1
```
Mam tu do czynienia z sekcją `where` w zagnieżdżonej funkcji. Warto tu przypomnieć, że w Haskellu obowiązuje leniwe obliczanie wartości. Oznacza, to, że wartości pomocnicze w sekcji `where` nie zostaną wyliczone, jeśli nie wymaga tego wyliczenie wartości głównego wyrażenia funkcji. Na przykład, jeśli w `fibhelper` `k==n` jest spełnione, to wartości `fib_k_next` i `k_next` nie zostaną obliczone.

### Wyrażenie let

Pokrewnym do sekcji `where` elementem składni jest wyrażenie `let`. Wyrażenie `let` pozwala zdefiniować wartości pomocnicze nie tylko dla funkcji, ale dla dowolnych wyrażeń. Składnia `let` jest trochę inna od składni `where`: najpierw podane są definicje wartości pomocniczych, potem słowo `in`, a potem właściwa wartość. Zademonstruję wyrażenie `let` robiąc kolejną modyfikację funkcji `fib2` z poprzedniej lekcji. Tym razem zamiast `where` użyję `let`.

```haskell
fib3 :: Int -> Int
fib3 n = let
    fibhelper :: Int -> Int -> Int -> Int
    fibhelper fib_k_1 fib_k k 
      | k == n = fib_k
      | otherwise = let
        fib_k_next = fib_k + fib_k_1
        k_next = k + 1
        in fibhelper fib_k fib_k_next k_next
  in fibhelper 1 1 1
```

Najbardziej wyraźną różnicą wizualną pomiędzy let a where jest to, że w `let` definicje pomocnicze poprzedzają wyrażenie, gdzie są użyte, a w przypadku `where` definicje pomocnicze są po właściwym wyrażeniu. Z kolei, największą różnicą z punktu widzenia składni języka jest fakt, że `let` jest wyrażeniem i można go użyć w dowolnym miejscu, np. przy wyliczeniu pojedynczego argumentu funkcji, a `where` można stosować jedynie w punkcie definicji funkcji. 

Bez tych elementów składniowych język Haskell byłby kompletny, a ich jedynym celem jest zwiększenie czytelności kodu źródłowego.

## Podsumowanie

To była krótka lekcja, mająca jedynie pokazać możliwości składni:

```haskell
fun args = def
    where
      val1 = x
      val2 a b = a + b
```

```haskell
let
 a = 3
 b = xyz
 in a + x
```

[Plik źródłowy z kodem z tej lekcji]({{< resource "Lekcja6.hs" >}})

Na końcu tej lekcji nie ma ćwiczeń. 

{-# OPTIONS_GHC -Wall #-}
module Lekcja5 where

fib2 :: Int -> Int
fib2 n = fibhelper 1 1 1
  where
    fibhelper :: Int -> Int -> Int -> Int
    fibhelper fib_k_1 fib_k k 
      | k == n = fib_k
      | otherwise = fibhelper fib_k fib_k_next k_next
      where
        fib_k_next = fib_k + fib_k_1
        k_next = k + 1

fib3 :: Int -> Int
fib3 n = let
    fibhelper :: Int -> Int -> Int -> Int
    fibhelper fib_k_1 fib_k k 
      | k == n = fib_k
      | otherwise = let
        fib_k_next = fib_k + fib_k_1
        k_next = k + 1
        in fibhelper fib_k fib_k_next k_next
  in fibhelper 1 1 1



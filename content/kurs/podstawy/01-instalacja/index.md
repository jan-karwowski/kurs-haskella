---
title: "Lekcja 1: Instalacja środowiska"
date: 2023-10-18T04:20:35+02:00
lessonnumber: 1
draft: false
---

## Narzędzia

Zanim rozpoczniemy naukę języka, potrzebne będą narzędzia, które pozwolą kompilować i uruchamiać programy w Haskellu. Ponadto niezastąpioną pomocą będzie instalacja i konfiguracja środowiska programisty, które będzie podpowiadać i podkreślać błędy. Zestaw narzędzi, z którego będziemy korzystać to:

GHC
: kompilator Haskella wraz z całym środowiskiem uruchomieniowym języka oraz interaktywny interpreter. W chwili pisania tego kursu GHC jest w zasadzie jedyną aktywnie rozwijaną implementacją Haskella.

Haskell Language Server
: serwer LSP obsługujący język Haskell

Stack
: w dużym skrócie narzędzie, które buduje projekty napisane w Haskellu -- ściąga wszystkie zależności, kompiluje je, a następnie buduje właściwy program. Stack nie będzie używany w początkowej części kursu, wyjaśnienie tego narzędzia pojawi się w odpowiednim czasie.

## Instrukcja instalacji

GHC zmienia się dynamicznie, dość często pojawiają się nowe wersje. Co więcej, kolejne wersje mogą zawierać zmiany niekompatybilne wstecz, które spowodują, że program napisany dla starszej wersji GHC nie skompiluje się w nowszej. Na szczęście, jeśli chodzi o podstawy języka, w tym obszarze sytuacja jest stabilna, materiał omawiany w tym kursie nie wymaga szczególnie nowych wersji kompilatora. W związku z tą zmiennością nie polecam instalowania narzędzi do Haskella z repozytorium pakietów używanej dystrybucji Linuksa. W wielu przypadkach wersje te będą sporo starsze niż bieżąca wersja GHC. 

Najczęściej polecaną w tej chwili metodą instalacji, która działa pod Linuksem i pod WSL2 jest użycie [GHCup](https://www.haskell.org/ghcup/).
Aby zainstalować wszystkie potrzebne programy należy:

 - zainstalować GHCup według instrukcji ze [strony głównej narzędzia: https://www.haskell.org/ghcup](https://www.haskell.org/ghcup/). 
   
   - W trakcie instalacji GHCup zostaniemy zapytani czy chcemy zainstalować HLS (Haskell Language Server). Można to potwierdzić. Alternatywnie można też preprowadzić instalację w kolejnym kroku.
 
 - Uruchomić `ghcup tui` w terminalu. Pojawi się interaktywny interfejs do instalacji narzędzi. W dolnej linii terminala widać instrukcję użycia. Wszystkie narzędzia najlepiej zainstalować w wersji oznaczonej jako _remommended_, w nawiasach podane wersje recommended w momencie pistania tego kursu. Potrzeba zainstalować:
 
    - Stack (2.13.1)
	- HLS (2.4.0.0) o ile nie był zainstalowany w trakcie instalacji GHC
	- GHC (9.4.7); tu ważna uwaga: wsparcie w edytorze z użyciem HLS będą miały tylko wersje oznaczone jako _hls-powered_.
  
 - GHCup pozwala na zainstalowanie kilku wersji tego samego narzędzia. Tylko jedna wersja może być tą, która jest uruchamiana przez odpowiadającą mu komendę, np `ghc`. Każda wersja dostępna jest pod nazwą, po której następuje numer wersji, np `ghc-9.4.7`. `ghcup tui` pozwala na ustawienie wersji domyślnej z użyciem polecenia _set_, uruchamianego klawiszem `s`. Wersje zainstalowane są oznaczone w tym interfejsie z użyciem zielonego znaku ✓ na lewo od wersji. Podwójny znak ✓ oznacza wersję domyślną. Jeśli któraś z zainstalowanych wersji nie jest domyślna, ustawiamy ją jako taką.
 
Pozostaje sprawdzić, czy zainstalowane przez nas komponenty działają. Najpierw sprawdzimy instalację GHC poprzez uruchomienie interpretera `ghci`. 

W terminalu wpisujemy polecenie `ghci`, powinna pojawić się zachęta podobna do tej:

```
GHCi, version 9.4.7: https://www.haskell.org/ghc/  :? for help
ghci>
```

Czym jest ghci i jak go używać zostanie omówione w kolejnych lekcjach. Na razie najbardziej podstawowe informacje: 

1. ghci wylicza wartości wyrażeń w języku Haskell,
2. aby zamknąć ghci trzeba wydać komendę `:q`
3. aby uzyskać pomoc, zgodnie z powitalnym komunikatem, trzeba wydać komendę `:?`


Sprawdźmy zatem czy wszystko działa. Zażądajmy, żeby interpreter obliczył wartość najprostszego wyrażenia pod słońcem, wpisując taką linię:

```
1+1
```

powinna pojawić się odpowiedź
```
2
```

Jeśli wszystko jest OK, to zamykamy interpreter:
```
:q
```

### Konfiguracja edytora

Generalnie zakładam, że kurs jest skierowany do osób, które już programują i mają swój ulubiony edytor/środowisko programisty. W prawie każdym współczesnym edytorze są mechanizmy pozwalające na korzystanie z serwerów LSP i trzeba znaleźć mechanizm właściwy dla swojego edytora i serwera HLS. Niezdecydowanym proponuję [VS Code](https://code.visualstudio.com/) z wtyczką [Haskell](https://marketplace.visualstudio.com/items?itemName=haskell.haskell).


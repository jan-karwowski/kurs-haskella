module Lekcja07 where

data Person = Person
  { firstName :: String,
    lastName :: String,
    address :: String,
    age :: Int
  }

data Point = Pt
  { getX :: Double,
    getY :: Double
  }

-- Ten fragment nie skompiluje się z powodu kolizji nazw
-- data Point3D = Pt3D
--   { getX :: Double,
--     getY :: Double,
--     getZ :: Double
--   }


data Point3D = Pt3D
  { getX3D :: Double,
    getY3D :: Double,
    getZ3D :: Double
  }

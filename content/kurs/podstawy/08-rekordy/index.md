---
title: "Lekcja 7: Rekordy"
date: 2024-01-26T15:16:17+01:00
draft: false
lessonnumber: 8
---

Na ostatniej lekcji poznaliśmy składnię pozwalającą definiować własne typy danych. Osobom, które widziały takie języki programowania jak C, Java, czy Python, mogą zauważyć, że składnia definiowania produktu ma poważną wadę, jeśli chodzi o czytelność kodu. Definiując punkt pisałem `Point = Pt Double Double` wiemy, że punkt składa się z dwóch instancji typu `Double`, ale opis ten w żaden sposób nie naprowadza nas an to co te punkty znaczą. W tym przypadku każdy, kto ma za sobą lekcje geometrii podejrzewa, że pierwsza składowa to X, a druga to Y, ale to tylko przypuszczenie. Co więcej, gdyby zdefiniować typ opisujący człowieka, który ma imię, nazwisko, adres i wiek, to sprawa robi się trudniejsza:

```haskell
data Person = Person String String String Int
```

W tym przypadku naprawdę nie sposób odgadnąć co oznaczają poszczególne składowe widząc taki kod po raz pierwszy.

## Składnia rekordu

Odpowiedzą na ten problem jest składnia rekordu, specjalny sposób definiowania typu będącego produktem:
```haskell
data Person = Person
  { firstName :: String,
    lastName :: String,
    address :: String,
    age :: Int
  }
```

Słowo kluczowe `data`, nazwa typu `Person` i nazwa konstruktora `Person` są analogiczne jak poprzednio. Tym razem, inaczej niż w przypadku punktu, pozwoliłem sobie na nazwanie tak samo typu i jego jedynego konstruktora, to nie ma znaczenia. Różnica w stosunku do podstawowej składni jest dalej. Zamiast wymienienia typów składowych produktu następują nawiasy klamrowe, a wewnątrz sekwencja wpisów `nazwa::Typ`, każdy z nich oddzielony przecinkiem. W ten sposób można nazwać każdą składową produktu. 

Oprócz wprowadzenia nazw dla składowych, rekordy dostarczają jeszcze jednej świetnej funkcjonalności --- generują funkcje dostępowe do składowych. Każda nazwa staje się jednocześnie funkcją dostępową do swojej składowej. Na przykład dla składowej `firstName` automatycznie zostanie wygenerowana funkcja o sygnaturze:
```
firstName :: Person -> String
```
która będzie w wyniku dawać wartość składowej `firstName`. Jeśli przypomnimy sobie, poprzednią lekcję, to dla punktu definiowaliśmy funkcje dostępowe ręcznie:
```haskell
getX :: Point -> Double
getX (Pt x y) = x
```

Znając składnię rekordu moglibyśmy teraz zdefiniować typ punkt ponownie, bez ręcznego dopisywania funkcji dostępowych do X i Y:

```haskell
data Point = Pt
  { getX :: Double,
    getY :: Double
  }
```

Może się wydawać, że to dziwne, że nazwałem składowe `getX` i `getY`. Zrobiłem to po to, żeby utrzymać kompatybilność z definicją z poprzedniej lekcji. Nazwy składowych rekordu są jednocześnie nazwami funkcji dostępowych. Gdybym nazwał je `x` i `y`, to funkcje dostępowe nazywałby by się też `x` i `y`. Moim celem było zdefiniowanie typu i akcesorów, które dokładnie zastąpią definicję z poprzedniej lekcji. Wszystkie funkcje stosujące dopasowanie argumentów dalej zadziałają z taką definicją, definiowanie typu z użyciem składni rekordu nie wyklucza używania potem dopasowania argumentów zamiast korzystania z akcesorów.

## Problemy z rekordami

Fakt, że rekord ma automatycznie zdefiniowane funkcje dostępowe może czasem prowadzić do problemów z kolizjami nazw.
Mając już zdefiniowany powyżej typ `Point` chcę dodać do mojego programu geometrię trójwymiarową. W związku z tym chciałbym utworzyć typ `Point3D` ze składowymi `x`, `y`, i `z`. Aby zachować spójność z definicją dwuwymiarowego punktu, zdecydowałem się jednak, że współrzędne nazwę `getX`, `getY`, `getZ`. Dodaję do mojego pliku następujący fragment kodu:

```haskell
data Point3D = Pt3D
  { getX :: Double,
    getY :: Double,
    getZ :: Double
  }
```

Ta niewinna definicja skutkuje jednak dwoma błędami kompilacji:
 - wielokrotna definicja funkcji `getX`,
 - wielokrotna definicja funkcji `getY`.
 
Co się stało? Automatycznie generowana funkcja dostępowa jest funkcją, jak każda inna i jej nazwa musi być unikatowa w obrębie modułu (póki co umiemy tylko zdefiniować jeden moduł w jednym pliku, więc oznacza to, że w obrębie pliku może być tylko jedna funkcja `getX` i tylko jedna funkcja `getY`. W tej chwili definicja typu `Point`
dostarcza funkcję
```haskell
getX :: Point -> Double
```
a typ `Point3D` dostarcza funkcję
```haskell
getX :: Point3D -> Double
```

W Haskellu dwie funkcje o tej samej nazwie w obrębie modułu są niedopuszczalne. Stąd błąd kompilacji. Jedynym sposobem na rozwiązanie tego problemu jest wymyślenie innych nazw składowych lub umieszczenie typów `Point` i `Point3D` w odrębnych modułach. Ja zdecyduję się na pierwszy sposób:

```haskell
data Point3D = Pt3D
  { getX3D :: Double,
    getY3D :: Double,
    getZ3D :: Double
  }
```

## Podsumowanie

Język Haskell byłby zupełnie kompletny, (to znaczy, że każdy program, który używa rekordów, można też zapisać bez nich), bez składni rekordów, ale możliwość ich stosowania poprawia czytelność programów. Składnia definicji rekordu trochę przypomina definiowania typów funkcji wewnątrz typów. 

## Zadania

1. Zdefiniować typ danych reprezentujący autora, który skłąda się z imienia i nazwiska oraz typ danych reprezentujący książkę, złożony z autora, tytułu i roku wydania. Napisać funkcję, która dla podanej książki wylicza imię autora.

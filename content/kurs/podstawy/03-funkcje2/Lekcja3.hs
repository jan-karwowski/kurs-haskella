{-# OPTIONS -Wall #-}
module Lekcja3 where

isZero :: Int -> Bool
isZero 0 = True
isZero x = False

testfun :: Int -> Int
testfun 1 = 3
testfun 2 = 2
testfun 3 = 1

signum :: Double -> Int
signum x
  | x > 0 = 1
  | x < 0 = -1
  | otherwise = 0

signum2 :: Double -> Int
signum2 x
  | x > 0 = 1
  | x < 0 = -1

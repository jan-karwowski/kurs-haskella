---
title: "Lekcja 3: Funkcje część 2"
date: 2023-10-20T10:55:05+02:00
lessonnumber: 3
draft: false
---

## Dopasowanie do wartości argumentów

W poprzedniej lekcji definiowaliśmy funkcje które miały dokładnie jedną definicję. Haskell pozwala zdefiniować kilka wariantów definicji jednej funkcji. Warianty te będą wybierane na podstawie dopasowania argumentów. To brzmi niejasno, więc przykład:
```haskell
isZero :: Int -> Bool
isZero 0 = True
isZero x = False
```
W powyższym kodzie mamy deklarację typu funkcji `isZero`, po czym, w dwóch kolejnych liniach, dwie różne definicje tejże funkcji. Trzeba zwrócić uwagę na fakt, że w pierwszej definicji w miejscu argumentu funkcji nie jest podana nazwa, ale `0`, czyli konkretna wartość typu `Int`. W drugiej definicji mamy natomiast nazwę `x`. Gdy jakaś funkcja ma wiele definicji w momencie wyliczenia wartości wszystkie definicje są przeglądane od góry do dołu według pozycji w kodzie źródłowym. Każda definicja porównywana jest z argumentami. Jeśli argumenty pasują, to brana jest ta definicja i kolejne nie są sprawdzane. 

Na przykładzie `isZero`: jeśli wyliczamy wartość `isZero 3`, najpierw następuje sprawdzenie czy wartości argumentów pasują do definicji `isZero 0`. Nie pasuje, bo 3 nie jest równe 0. Rozpatrywana jest więc kolejna definicja, czyli `isZero x`. Tu mamy definicję, w której `x` może być dowolną liczbą. Ta definicja pasuje, więc zostaje wybrana. Wynikiem jest `False`. Jeśli zapytamy o wyliczenie wartości `isZero 0`, to pasować będzie i pierwsza i druga definicja. Definicja `isZero 0` zostanie wybrana, bo znajduje się wcześniej w kodzie źródłowym.

Warto oczywiście sprawdzić, czy funkcja działa. Tworzę plik `Lekcja3.hs` o następującej zawartości:
```haskell
module Lekcja3 where

isZero :: Int -> Bool
isZero 0 = True
isZero x = False
```
Tak jak w lekcji, otwieram terminal, przechodzę do katalogu, gdzie zapisałem plik, uruchamiam `ghci` i wydaję polecenie `:loda Lekcja3.hs`
Testuję funkcję `isZero` dla kilku wartości: `isZero 0`, `isZero 5`, `isZero (-1)`.

Nie ma żadnych ograniczeń na to ile definicji jednej funkcji jest zaimplementowanych nie jest też wymagane, aby były to warianty wzajemnie się wykluczające, jak już napisałem, jeśli jakieś argumenty funkcji pasują do kilku definicji, zostanie wybrana pierwsza definicja według kolejności pojawiania się w kodzie źródłowym. Należy o tym pamiętać, bo pomyłka polegająca na zamianie kolejnością implementacji funkcji `isZero`
```haskell
isZero :: Int -> Bool
isZero x = False
isZero 0 = True
```
spowoduje, że funkcja będzie zawsze zwracać `False`. Sprawdźmy to nanosząc zmiany do pliku `Lekcja3.hs` i wydając polecenie `:reload` w interpreterze. 

### Funkcje częściowe

Jedną z często krytykowanych cech języka Haskell jest możliwość zdefiniowania funkcji częściowej, to znaczy takiej, która jest zdefiniowana tylko dla niektórych argumentów z dziedziny. Zdefiniujmy funkcję `testfun` następującej postaci:
```haskell
testfun :: Int -> Int
testfun 1 = 3
testfun 2 = 2
testfun 3 = 1
```
taka funkcja jest dopuszczalna przez język. Dopiszę ją teraz do pliku `Lekcja3.hs` i przeładuję interpreter poleceniem `:reload`. 
Mogę spróbować wyliczyć w interpreterze wartość funkcji:
```haskell
testfun 1
testfun 2
testfun 3
testfun 0
```
Co obserwuję? Zgodnie z oczekiwaniami dla argumentów 1, 2, 3 dostaję spodziewany wynik zgodny z definicją. W przypadku argumentów, dla których nie zdefiniowałem funkcji, otrzymuję komunikat:
```
*** Exception: Lekcja3.hs:(8,1)-(10,13): Non-exhaustive patterns in function testfun
```
Komunikat wskazuje mi miejsce, gdzie w kodzie źródłowym zostały podane definicje funkcji `testfun` i informuje, że te definicje nie obejmują wszystkich możliwych argumentów. 

*W tym kursie będę popierał pogląd, że funkcje częściowe są błędem projektowym i nie należy takich definiować.* Jednocześnie trzeba zauważyć, że funkcje częściowe można znaleźć nawet w podstawowych funkcjach dostarczanych przez język, więc nie uciekniemy od nich.

## Definicja wariantów funkcji z użyciem wartowników

Innym sposobem zdefiniowania kilku wariantów funkcji jest użycie wartowników, czyli mechanizmu składniowego, który pozwala wybrać jeden z wariantów na podstawie warunku logicznego. Zdefiniuję teraz funkcję `signum`, której wynikiem będzie 1, jeśli argument jest dodatni, -1, jeśli argument jest ujemny i 0, jeśli argument nie jest ani dodatni ani ujemny:
```haskell
signum :: Double -> Int
signum x
  | x > 0 = 1
  | x < 0 = -1
  | x == 0 = 0
```

Składnia wartowników , w porównaniu z innymi językami programowania,jest nietypowa ale można się do niej przyzwyczaić, definicja ma następującą strukturę:

1. nazwa funkcji i argumenty, dokładnie tak samo jak zwykła definicji,
2. potem **nie ma** znaku `=`
3. potem, każdy wariant w nowej linii **oraz** każdy wariant wcięty względem linii z nazwą i argumentami, składający się z
   1. pionowej kreski `|`,
   2. warunku logicznego,
   3. znaku `=`,
   4. wartości funkcji dla przypadku, gdy warunek wartownika jest prawdziwy.
Wariantów może być dowolnie dużo, a minimalnie 1. 

Zasada działania jest podobna, jak w przypadku kilku wariantów definicji funkcji: środowisko uruchomieniowe sprawdza od góry do dołu wszystkich wartowników i używa pierwszego od góry, którego warunek jest prawdziwy. To oznacza, że dopuszczalne jest, gdy kilka wartowników ma prawdziwy warunek. Wtedy wybierany jest pierwszy pasujący wariant od góry. To pozwala zwykle zastąpić ostatni warunek stałą `True`:
```haskell
signum :: Double -> Int
signum x
  | x > 0 = 1
  | x < 0 = -1
  | True = 0
```
W przypadku funkcji signum zysk w długości programu jest nieduży, ale łatwo sobie wyobrazić, że warunki mają długość 60 znaków lub więcej, wtedy taka zmiana skróci program. Dodatkowo, żeby poprawić (subiektywną) estetykę programu, można wykorzystać wartość `otherwise` zdefiniowaną w języku:
```haskell
otherwise :: Bool
otherwise = True
```
Jest to nic innego, jak nadanie wartości True innej nazwy pomocniczej. Wtedy funkcja signum może wyglądać "ładniej": 
```haskell
signum :: Double -> Int
signum x
  | x > 0 = 1
  | x < 0 = -1
  | otherwise = 0
```

Składnię wartowników można stosować do definiowania funkcji również wtedy, gdy jest wiele różnych definicji, tak w funkcji `isZero`.

Podobnie, jak w przypadku wielu wariantów funkcji, w przypadku wartowników można również zdefiniować funkcję częściową, wraz ze wszystkimi jej problemami. Zobaczymy taką funkcję:
```haskell
signum2 :: Double -> Int
signum2 x
  | x > 0 = 1
  | x < 0 = -1
```

Co się stanie, gdy zapytamy o wartość tej funkcji dla argumentu 0? Otrzymujemy podobny błąd jak w przypadku funkcji częściowej zdefiniowanej przez wiele wariantów.


## Opcje kompilatora -- ostrzeżenia

Haskell niestety pozwala na definiowanie funkcji częściowych, ale mimo to dostępne są pewne mechanizmy obrony. Podobnie, jak np. w języku `C`, gdzie dopuszczalne jest pisanie kodu z oczywistymi błędami, ratunkiem jest włączenie dodatkowych ostrzeżeń kompilacji. W przypadku kompilatora `ghc` będziemy stosować flagę `-Wall`, która włącza ostrzeganie o większości problemów, które `ghc` jest w stanie wykryć, a które nie są krytycznymi błędami kompilacji.  Typowym podejściem jest włączenie flagi `-Wall` dla całego projektu. W tym momencie kursy operujemy jednak na pojedynczych plikach, a nie na całym projekcie. Jak włączyć flagę `-Wall` dla pojedynczego pliku?

1. Jeśli uruchamiam interpreter `ghci` z linii poleceń, to mogę go uruchomić z opcją `-Wall`:
   ```
   ghci -Wall
   :l Lekcja3.hs
   ```
   dzięki temu dla każdego ładowanego do interpretera pliku będą raportowane ostrzeżenia.
   W tej chwili mój plik `Lekcja3.hs` ma następującą zawartość: 
   ```haskell
   module Lekcja3 where

   isZero :: Int -> Bool
   isZero 0 = True
   isZero x = False

   testfun :: Int -> Int
   testfun 1 = 3
   testfun 2 = 2
   testfun 3 = 1

   signum :: Double -> Int
   signum x
     | x > 0 = 1
     | x < 0 = -1
     | otherwise = 0

   signum2 :: Double -> Int
   signum2 x
     | x > 0 = 1
     | x < 0 = -1
   ```
   Dla tak zdefiniowanych funkcji otrzymałem następujące ostrzeżenia:

   ```
   Lekcja3.hs:6:8: warning: [-Wunused-matches]
       Defined but not used: ‘x’
     |
   6 | isZero x = False
     |        ^

   Lekcja3.hs:9:1: warning: [-Wincomplete-patterns]
       Pattern match(es) are non-exhaustive
       In an equation for ‘testfun’:
           Patterns of type ‘Int’ not matched:
               p where p is not one of {3, 1, 2}
     |
   9 | testfun 1 = 3
     | ^^^^^^^^^^^^^...

   Lekcja3.hs:20:1: warning: [-Wincomplete-patterns]
       Pattern match(es) are non-exhaustive
       In an equation for ‘signum2’:
           Patterns of type ‘Double’ not matched: _
      |
   20 | signum2 x
      | ^^^^^^^^^
   ```
   Ostrzeżenia `incomplete-patterns` wskazują na umówione wcześniej problemy. 
2. Drugim sposobem jest ustawienie flag kompilacji dla pliku. W tym celu w pierwszej linii pliku, przed definicją modułu należy wstawić linię `{-# OPTIONS -Wall #-}`, która ustawi opcję `-Wall` jako opcję kompilacji tego konkretnego pliku. To podejście jest lepsze od podejścia pierwszego, bo powoduje, że flaga `-Wall` jest również wykorzystywana przez `hls`, czyli serwer LSP podpowiadający i weryfikujący kod Haskella w edytorze. W ten sposób ostrzeżenia będą sygnalizowane już w edytorze. 

Po dodaniu linii włączającej ostrzeżenia plik `Lekcja3.hs` wygląda tak:
```haskell
{-# OPTIONS -Wall #-}
module Lekcja3 where

isZero :: Int -> Bool
isZero 0 = True
isZero x = False

testfun :: Int -> Int
testfun 1 = 3
testfun 2 = 2
testfun 3 = 1

signum :: Double -> Int
signum x
  | x > 0 = 1
  | x < 0 = -1
  | otherwise = 0

signum2 :: Double -> Int
signum2 x
  | x > 0 = 1
  | x < 0 = -1
```

## Podsumowanie
W tej lekcji poznaliśmy:
 - kilka definicji jednej funkcji w zależności od argumentów
 - definicje wariantów funkcji z użyciem wartowników
 - sposób na włączenie dodatkowych ostrzeżeń kompilatora.

[Plik źródłowy z kodem z tej lekcji]({{< resource "Lekcja3.hs" >}})

## Ćwiczenia

1. Napisać z użyciem wartowników funkcję `absolute :: Double -> Double`, która wylicza wartość bezwzględną liczby.
2. Napisać z użyciem wartowników funkcję `maximum :: Double -> Double -> Double -> Double`, której wartością jest wartość maksymalnego z jej trzech argumentów.

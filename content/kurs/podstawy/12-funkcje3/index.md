---
title: "Lekcja 12: Funkcje 3"
 date: 2025-01-28T21:56:16+01:00
draft: true
lessonnumber: 12
---

W tej lekcji pokażę bardziej zaawansowane elementy składni Haskella związane z definiowaniem funkcji.
Nie omawiałem ich dotąd, bo nie było potrzeby aby ich użyć. 
W kolejnej lekcji, dotyczącej operacji na listach przydadzą się jednak dodatkowe mechanizmy przedstawione tu.

# Definiowanie funkcji z użyciem mniejszej liczby argumentów

W [lekcji 2]({{< ref ../02-funkcje/index.md >}}) mówiłem o tym, że funkcje w Haskellu są jednoargumentowe.
Jednocześnie przykład definicji `plus` w tamtej lekcji zdecydowanie bardziej przypominał funkcję dwuargumentową.
Haskell pozwala również na definicje funkcji, które mają mniej argumentów niż wynikałoby to z ich typu.
Omówię ten mechanizm składni na przykładzie.
Przypomnę teraz funkcje z [lekcji 2]({{< ref ../02-funkcje/index.md >}}):

```haskell
plus :: Int -> Int -> Int
plus x y = x + y

addone :: Int -> Int
addone x = plus 1 x
```

Funkcję `addone` można również zdefiniować w następujący sposób
```haskell
addone :: Int -> Int
addone = plus 1
```

Typ funkcji jest dokładnie taki sam, jak w poprzedniej deklaracji. 
Definicja funkcji jest inna.
W porównaniu z poprzednią, brakuje tutaj argumentu `x`, a mimo to taki kod kompiluje się bez błędów.
Żeby całość była poprawna składniowo, ważny jest typ wyrażenia po prawej stronie znaku równości w definicji funkcji.
W pierwszym przypadku było do `plus 1 x`, czyli wyrażenie typu `Int`.
W przypadku, który omawiam teraz, wyrażenie `plus 1` ma typ `Int -> Int`.
Przypomnę: funkcja `plus` ma typ `Int -> Int -> Int`, co możemy zapisać jako `Int -> (Int -> Int)` (ta składnia jest opisana w [lekcji 2]({{< ref ../02-funkcje/index.md >}}).
W związku z czym aplikacja funkcji `plus` wartością `1` ma wynik typu `Int -> Int`.

Definicja funkcji jest w Haskellu poprawna, gdy typ wyrażenia po prawej stronie zgadza się z typem funkcji z którego zostało obciętych z lewej strony tyle typów argumentów ile argumentów jest wymienionych w definicji funkcji.
Gdybyśmy rozważali funkcję czteroargumentową, to definicję można by napisać na jeden z poniższych sposobów:

```haskell
fun4 :: A -> B -> C -> D -> E
-- Wariant 1
fun4 = k -- k jest typu A -> B -> C -> D -> E
-- Wariant 2
fun4 x = l -- l jest typu B -> C -> D -> E
-- Wariant 3
fun4 x y = m -- m jest typu C -> D -> E
-- Wariant 4
fun4 x y z = n -- n jest typu D -> E
-- Wariant 5
fun4 x y z w = o -- o jest typu E
```
Uwaga: skopiowanie całego powyższego bloku spowoduje błąd kompilacji.
Poprawne jest użycie jednej definicji na raz.

## Redukcja η (eta)

Redukcja η (eta)[^1] jest transformacją kodu Haskellowego, która polega na usunięciu ostatniego argumetnu definicji funkcji i ostatniego argumetnu funkcji w wyrażeniu liczącym wartość, jeśli taka zmiana nie zmieni wartości funkcji, na przykład:
```haskell
addone :: Int -> Int
addone x = plus 1 x
```
W tej definicji x jest ostatnim argumentem podanym po lewej stronie znaku równości.
Jednocześnie wartość x jest podana jak ostatni argument funkcji `plus`, która jest używana w wyliczeniu.
Można zatem zastosować redukcję eta i usunąć `x` po obu stronach znaku równośći:
```haskell
addone :: Int -> Int
addone = plus 1
```
Taka definicja jest równoważna poprzedniej.

Nie zawsze da się zastosować redukcję eta, roważmy teraz funkcję podwajającą x:
```haskell
doubled :: Int -> Int
doubled x = plus x x
```

W tej sytuacji nie można wykonać redukcji eta, ponieważ `x` występuje nie tylko jako ostatni argument wyrażenia definiująca wartość funkcji.

W powszechnie przyjętych zasadach formatowania kodu Haskella, należy stosować tę redukcję, jeśli jest to możliwe. 
HLS domyślnie sugeruje taką zmianę.

## Ograniczenia

W poprzednich lekcjach pokazałem, że Haskell pozwala na definiowanie kilku wariantów funkcji. 
Jeśli funkcja ma kilka wariantów, to wszystkie warianty muszą być zdefiniowane dla dokładnie tej samej liczby argumentów.
Sytuacja, gdy są warianty tej samej funkcji mają różne liczby argumentów w definicjach, jest błędem składniowym. 

```haskell
increaseIf3 :: Int -> Int -> Int -- powiększa drugi argument, jeśli pierwszy jest równy 3, w przeciwnym wypadku wynikiem jest 3
increaseIf3 3 x = plus 1 x
increaseIf3 y x = 3
```

Redukcja pierwszego wariantu do postaci `increaseIf3 3 = plus x` poskutkuje spowoduje błąd kompilacji.

# Funkcje lambda

Z racji tego, że w Haskellu, (jak w wielu współczesnych językach programowania), funkcja jest typem danych, może być też typem argumentu funkcji. 
Zaproponuję teraz przykładową funkcję, która będzie jak argument przyjmować inną funkcję.
Propozycja to funkcja `negated`, która przyjmuje jako argument funkcję `Int -> Int` oraz wartość typu `Int` i jako wynik daje zanegowany wynik oryginalnej funkcji dla podanego argumentu.

```haskell
negate :: (Int -> Int) -> Int -> Int
negate fun arg = -(fun arg)
```
Pod względem składniowym istotne jest to, że w definicji typu, typ opisujący funkcję jest w nawiasach `(Int -> Int)`.
Gdyby ten fragment nie był ujętny w nawiasy, to funkcja byłaby funkcją przyjmującą trzy argumenty typu Int, a typem wyniku byłby Int.

Mogę sprawdzić działanie tej funkcji z użyciem, na przykład, funkcji `addone`:
```haskell
negate addone 2 -- -3

negate addone 5 -- -6
```

W lekcji, która będzie prezentować operacje na listach, pojawi się dużo funkcji, które przyjmują inną funkcję jako argument.

Funkcja lamba, to sposób na zdefiniowanie funkcji TODO




[^1]: η to litera greckiego alfabetu

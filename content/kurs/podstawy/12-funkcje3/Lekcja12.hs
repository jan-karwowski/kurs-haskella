module Lekcja12 where

plus :: Int -> Int -> Int
plus x y = x + y

addone :: Int -> Int
addone x = plus 1 x

doubled :: Int -> Int
doubled x = plus x x


increaseIf3 :: Int -> Int -> Int -- powiększa drugi argument, jeśli pierwszy jest równy 3, w przeciwnym wypadku wynikiem jest 3
-- increaseIf3 3 = plus 1 -- Błąd!
increaseIf3 3 x = plus 1 x
increaseIf3 y x = 3

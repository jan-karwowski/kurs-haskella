---
title: "Intro"
date: 2023-10-18T03:45:19+02:00
draft: false
---

Ten kurs Haskella jest skierowany dla osób, które nigdy w Haskellu nie programowały, ale mają przynajmniej niewielkie doświadczenie z programowanie imperatywnym, np. w C, Pythonie lub Javie. Czym jest programowanie imperatywne? Jest to najbardziej rozpowszechnione podejście do programowania, gdzie program wykonuje kolejne instrukcje jedna po drugiej i każda instrukcja może powodować jakiś efekt (na przykład wypisanie tekstu czy przypisanie do zmiennej).
Kurs podzielony jest na niewielkie lekcje, które pozwalają w spokojnym tempie poznawać język. Materiały przeznaczone są dla osób, które już zdecydowały się uczyć Haskella, nie ma tu materiałów przekonujących, że właśnie Haskell jest językiem wartym nauczenia się (moim zdaniem jest), nie ma porównań z innymi językami.

## Co to znaczy "powolny"?

Z jednej strony ten kurs nie narzuca dużego tempa nauki. Niektórzy czytelnicy zapewne będą robić kilka lekcji na raz. Podział pozwala na uczenie się Haskella osobom, które mogą przeznaczyć mało czasu w tygodniu na naukę. 

Drugą cechą, którą też można opisać słowem powolny, jest fakt, że ten kurs nie jest nastawiony na szybkie uzyskanie umiejętności pozwalających pisać przydatne programy. Zamiast takiego podejścia często skupiam się najpierw na podstawach działaniu i na pisaniu wprawek pozwalających te podstawy zrozumieć i utrwalić.

## Struktura kursu

Kurs jest podzielony na lekcje, które omawiają pojedynczy temat. Większość lekcji jest krótka i na ich przerobienie powinno wystarczyć 20 minut. Pojedyncze lekcje, ze względu na trudniejszy materiał są bardziej rozbudowane, ale żadna lekcja nie wymaga więcej niż godziny.
Lekcje są zgrupowane w sekcje, w tej chwili jedyną sekcją są podstawy, ale docelowo pojawią się kolejne sekcje tematyczne.
